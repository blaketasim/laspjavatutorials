package brownBag_10_07_2016;

import static brownBag_10_07_2016.BrownBag_10_07_2016.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class BrownBag_10_07_2016_Test {
    
    
    @Test
    public void test_readFile() {
        String fileName = "test/brownBag_10_07_2016/textFile.txt";
        int nLinesToSkip = 1;
        int nLinesToRead = 10;
        String[] out = readFile(fileName, nLinesToSkip, nLinesToRead);
        
        String[] exp = {
                "line 2", 
                "", 
                "line 3", 
                "", 
                "line 4", 
                "", 
                "line :(", 
                "line 5", 
                "", 
                "line 6"};
        
        assertArrayEquals(exp, out);
    }
    
    @Test
    public void test_readFile_java7() {
        String fileName = "test/brownBag_10_07_2016/textFile.txt";
        int nLinesToSkip = 1;
        int nLinesToRead = 10;
        String[] out = readFile_java7(fileName, nLinesToSkip, nLinesToRead);
        
        String[] exp = {
                "line 2", 
                "", 
                "line 3", 
                "", 
                "line 4", 
                "", 
                "line :(", 
                "line 5", 
                "", 
                "line 6"};
        
        assertArrayEquals(exp, out);
    }
    
    @Test
    public void test_readFile_java8() {
        String fileName = "test/brownBag_10_07_2016/textFile.txt";
        int nLinesToSkip = 1;
        int nLinesToRead = 10;
        String[] out = readFile_java8(fileName, nLinesToSkip, nLinesToRead);
        
        String[] exp = {
                "line 2", 
                "", 
                "line 3", 
                "", 
                "line 4", 
                "", 
                "line :(", 
                "line 5", 
                "", 
                "line 6"};
        
        assertArrayEquals(exp, out);
    }
    
    @Test
    public void test_readFile_contains_and_does_not() {
        String fileName = "test/brownBag_10_07_2016/textFile.txt";
        String contains = "line";
        String doesNotContain = ":(";
        String[] out = readFile(fileName, contains, doesNotContain);
        
        String[] exp = {
                "line 1",
                "line 2", 
                "line 3", 
                "line 4", 
                "line 5", 
                "line 6", 
                "line 7", 
                "line 8", 
                "line 9"};
        
        assertArrayEquals(exp, out);
    }
    
    @Test
    public void test_arrayAdd() {
        double[] arr = {1, 2, 3, 4, 5, 6};
        double[] actual = arrayAdd(arr, 5);
        
        double[] exp = {6, 7, 8, 9, 10, 11};
        
        assertArrayEquals(exp, actual, 0);
    }
    
    @Test
    public void test_rms() {
        double[] diffs = {2, -2, 2, -2};
       
        double expRms = 2.0;
        double actualRms = rms(diffs);     
        assertEquals(expRms, actualRms, 0);
    } 
    
    @Test
    public void test_parseStringArrayToDoubles() {
        String[] strs = {"10.0", "11.1", "33.0"};
        
        double[] out = parseStringArrayToDoubles(strs);
        double[] exp = {10.0, 11.1, 33.0};
        
        assertArrayEquals(exp, out, 0);
    }
    
    @Test
    public void test_incrementArray() {
        double[] values = incrementArray(10.0, 14, 2);
        double[] expected = {10, 12, 14};
        assertArrayEquals(expected, values, 0);
    }
    
    @Test
    public void test_incrementArray_2() {
        double[] values = incrementArray(10.0, 14, 3);
        double[] expected = {10, 13};
        assertArrayEquals(expected, values, 0);
    }
    
    @Test
    public void test_incrementArray_3() {
        double[] values = incrementArray(10, 11, .2);
        double[] expected = {10, 10.2, 10.4, 10.6, 10.8, 11};
        assertArrayEquals(expected, values, 1E-12);
    }
    
    @Test
    public void test_incrementArray_4() {
        double[] values = incrementArray(10, 10.99, .2);
        double[] expected = {10, 10.2, 10.4, 10.6, 10.8};
        assertArrayEquals(expected, values, 1E-12);
    }
    
    @Test
    public void test_buildString() {
        String[] values = {"val 1", "val 2", "val 3"};
        String actual =  buildString(", ", values);
        String exp = "val 1, val 2, val 3";
        assertEquals(exp, actual);
    }
    
    @Test
    public void test_buildString_second() {
        String[] values = {"val 1", "val 2", "val 3"};
        String actual = buildString(" and ", values);
        String exp = "val 1 and val 2 and val 3";
        assertEquals(exp, actual);
    }
    
    @Test
    public void test_buildString_empty() {
        String[] values = {};
        String actual = buildString(" and ", values);
        String exp = "";
        assertEquals(exp, actual);
    }
    
    @Test
    public void test_buildString_with_null() {
        String[] values = {"val 1", null, "val 3"};
        String actual = buildString(" and ", values);
        String exp = "val 1 and null and val 3";
        assertEquals(exp, actual);
    }
    

    @Test
    public void test_max_double() {
        double[] in = {2, 4, 6, 1, 3, 2, 10, 2, 3, 4};
        
        double actual = max(in);
        double exp = 10;
        assertEquals(exp, actual, 0.0);
    }
    
    @Test
    public void test_replicateDouble() {
        double[] actual = replicateDouble(1.0, 4);
        double[] exp = {1d, 1d, 1d, 1d};
        assertArrayEquals(exp, actual, 0);
    }
    
    @Test
    public void test_replicateString() {
        String val = "hi";
        String[] actual = replicateString(val, 4);
        String[] exp = {"hi", "hi", "hi", "hi"};
        
        assertArrayEquals(exp, actual);
    }
    
    @Test
    public void test_list2PrimDoubleArray() {
        ArrayList<Double> list = new ArrayList<Double>();
        list.add(1.0);
        list.add(2.0);
        double[] actual = list2PrimDoubleArray(list);
        double[] exp = {1, 2};
        assertArrayEquals(exp, actual, 0);
    }
    
    @Test
    public void test_list2PrimDoubleArray_0() {
        ArrayList<Double> list = new ArrayList<Double>();
        double[] actual = list2PrimDoubleArray(list);
        double[] exp = {};
        assertArrayEquals(exp, actual, 0);
    }
    
}
