package brownBag_10_07_2016;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.function.IntBinaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

public class BrownBag_10_07_2016 {
    

    public static interface Hello {

        public void sayHi(); // previous

        public default void sayBye() {
            
        }
        
        public default String beNice() { // new default method allowed
            return "smile";
        }
        
        

        public static String handShake() { // new static method allowed
            return "Nice";
        }

    }
    
    public static class HelloBarry implements Hello {

        @Override
        public void sayHi() {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public void sayBye() { // default method can be overrided (sp?)
            
        }
     
        
    }

    public static double[] arrayAdd(double[] arr, double toAdd) {
        
        
            double[] rtn = DoubleStream.of(arr)
                    .map( (d) -> d + toAdd)
                    .toArray();


//        int n = arr.length;
//        double[] rtn = new double[n];
//        for (int i = 0; i < n; i++) {
//            rtn[i] = arr[i] + toAdd;
//        }

        return rtn;
    }

    public static double rms(double[] vals) {
        double store = DoubleStream.of(vals)
               .map( d -> d * d)
               .sum();
        
//        int n = vals.length;
//        double store = 0;
//
//        for (double val : vals)
//            store += val * val;

        store = store / vals.length;

        double rtn = java.lang.Math.sqrt(store);

        return rtn;
    }


    public static double[] parseStringArrayToDoubles(String[] stringsToParse) {
        double[] rtn = Stream.of(stringsToParse)
//                .mapToDouble( (s) -> Double.parseDouble(s))
                .mapToDouble(Double::parseDouble)
                .toArray();
        
        
//        int n = stringsToParse.length;
//        double[] rtn = new double[n];
//        for (int i = 0; i < n; i++)
//            rtn[i] = Double.parseDouble(stringsToParse[i]);
        return rtn;
    }

    public static double[] incrementArray(double startValue, double maxValue, double incrementSize) {
        Number nValues = Math.floor((maxValue - startValue) / incrementSize) + 1;
        int n = nValues.intValue();
        
        double[] rtn = DoubleStream.iterate(startValue, d -> d + incrementSize) //infinte steram
                .limit(n)
                .toArray();
        
//        double[] rtn = new double[n];
//
//        for (int i = 0; i < n; i++)
//            rtn[i] = startValue + incrementSize * i;

        return rtn;
    }

    public static double max(double[] array) {
            return DoubleStream.of(array)
                    .max()
                    .getAsDouble();
        
//        double[] temp = array.clone();
//        Arrays.sort(temp);
//        int n = temp.length;
//        return temp[n-1];
    }

    public static double[] replicateDouble(double valToReplicate, int nTimesToReplicate) {
        double[] rtn = DoubleStream.generate(() -> valToReplicate)
                .limit(nTimesToReplicate)
                .toArray();
        
//        double[] rtn = new double[nTimesToReplicate];
//        for (int i = 0; i < nTimesToReplicate; i++)
//            rtn[i] = valToReplicate;

        return rtn;
    }

    public static String[] replicateString(String valToReplicate, int nTimesToReplicate) {
        String[] rtn = Stream.generate(() -> valToReplicate)
                .limit(nTimesToReplicate)
//                .toArray(String[]::new);
                .toArray((s) -> new String[nTimesToReplicate]); 

        
        // does this make a second copy?
//        i believe it makes a list, then copies the data to the array
//         you can get a list out by using:
        
//        .collect(Collectors.toList());
        
        // vs an array.
        
        // if you use the following, the stream will print everything out, but then 
        // fail to write to the array because the array is not of the right size.
        
//        .peek(System.out::println)
//        .toArray((s) -> new String[1]); // this actually works too
        
        
        
        
        // does this make a second copy?
        
        
//        String[] rtn = new String[nTimesToReplicate];
//
//        for (int i = 0; i < nTimesToReplicate; i++)
//            rtn[i] = valToReplicate;

        return rtn;
    }

    public static String buildString(String separator, Object[] parts) {
        return Stream.of(parts)
//                .map(o -> o.toString())
                //                .map(Object::toString)
//                .map((o) -> {
//                    if (o == null)
//                        return "null";
//                    else
//                        return o.toString();
//                })
                .map(o -> Optional.ofNullable(o).orElse("null").toString())
                .collect(Collectors.joining(separator));    

        
//        int n = parts.length;
//        if (n == 0) 
//            return "";
//
//        StringBuilder rtn = new StringBuilder(parts[0].toString());
//        for (int i = 1; i < n; i++)
//            rtn.append(separator + parts[i]);
//
//        return rtn.toString();
    }


    public static String[] readFile(String fileName, int nLinesToSkip, int nLinesToRead) {
        ArrayList<String> holdList = new ArrayList<String>();

        File file = new File(fileName);
        BufferedReader reader = null;
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(file);
            reader = new BufferedReader(fileReader);
            String text;
            int i = 0;

            while ((text = reader.readLine()) != null) {
                if (nLinesToSkip <= i && i < ( nLinesToRead + nLinesToSkip)) {
                    holdList.add(text);
                }
                i++;
            }

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close(); //ensure reader is closed.
                }
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String[] out = new String[holdList.size()];
        out = holdList.toArray(out);
        return out;
    }

    public static String[] readFile_java7(String fileName, int nLinesToSkip, int nLinesToRead) {
       int x = 1_000;
        
        ArrayList<String> holdList = new ArrayList<String>();

        File file = new File(fileName);
        try ( 
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            ) {
                String text;
            int i = 0;

            while ((text = reader.readLine()) != null) {
                if (nLinesToSkip <= i && i < ( nLinesToRead + nLinesToSkip)) {
                    holdList.add(text);
                }
                i++;
            }

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        

        String[] out = new String[holdList.size()];
        out = holdList.toArray(out);
        return out;
    }

    public static String[] readFile_java8(String fileName, int nLinesToSkip, int nLinesToRead) {
        String[] out = null;
        
        try {
            out = Files.readAllLines(Paths.get(fileName)).stream()
                    .skip(nLinesToSkip)
                    .limit(nLinesToRead)
                    .toArray(String[]::new);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return out;
    }

    public static String[] readFile(String fileName, String contains, String doesNotContain) {
        String[] out = null;

        try {
            out = Files.readAllLines(Paths.get(fileName)).stream()
                    .filter((s) -> s.contains(contains) && !s.contains(doesNotContain))
                    .toArray(String[]::new);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return out;
    }

    public static double[] list2PrimDoubleArray(ArrayList<? extends Number> list) {
            return list.stream()
                    .mapToDouble(Number::doubleValue)
                    .toArray();
        
//        int n = list.size();
//        double[] rtn = new double[n];
//        Number[] array = new Number[n];
//        array = list.toArray(array);
//        for (int i = 0; i < n; i++)
//            rtn[i] = array[i].doubleValue();
//        return rtn;
    }





    public static void print(int number, Predicate<Integer> predicate, String msg) {
        System.out.println(number + " " + msg + ":" + predicate.test(number));
    }

    public static String superSlowMethod() {
        System.out.println("***************  this is really slow... ish");

        return "ouch";
    }

    public static void log(Supplier<String> in, boolean shouldLog) {
        if (shouldLog)
            System.out.println(in.get());

    }

    public static void log(String in, boolean shouldLog) {
        if (shouldLog)
            System.out.println(in);
    }

    public static void main(String[] args) {
        System.out.println("printing");

        // 
        Builder<String> strBuilder = Stream.builder();
        strBuilder.add("hi").add(superSlowMethod()).add("bye");
        
        strBuilder.build()
        .forEachOrdered(System.out::println);
        
        
//        log(superSlowMethod(), true);
        
        log(() -> superSlowMethod(), false);

        Predicate<Integer> isEven = i -> i % 2 == 0;
        Predicate<Integer> isGT4 = i -> i > 4;
        
        print(5, isEven, "isEven?");
        print(10, isEven, "isEven?");

        print(2, isGT4, "isGT4?");
        print(10, isGT4, "isGT4?");
        
        print(5, isEven.and(isGT4), "is even and gt 4?");
        print(10, isEven.and(isGT4), "is even and gt 4?");

    }
}
